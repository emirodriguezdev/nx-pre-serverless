const { MongoMemoryServer } = require('mongodb-memory-server');
const mongoose = require('mongoose');
const request = require('supertest');
const app = require('./src/app');

let mongo;
beforeAll(async () => {
  process.env.JWT_SECRET = 'test';

  mongo = await MongoMemoryServer.create();
  const mongoUri = mongo.getUri();

  await mongoose.connect(mongoUri, {});
});

beforeEach(async () => {
  const collections = await mongoose.connection.db.collections();

  for (let collection of collections) {
    await collection.deleteMany({});
  }
});

afterAll(async () => {
  if (mongo) {
    await mongo.stop();
  }
  await mongoose.connection.close();
});

global.signin = async () => {
  const email = 'test@test.com';
  const password = 'password';

  const response = await request(app)
    .post('/api/users')
    .send({
      email,
      password,
    })
    .expect(201);

  return response.body.userJwt;
};

global.signinShop = async () => {
  const email = 'testShop@test.com';
  const password = 'password';

  const response = await request(app)
    .post('/api/users')
    .send({
      email,
      password,
    })
    .expect(201);

  await request(app)
    .post('/api/shops')
    .send({
      name: 'test',
    })
    .set('Authorization', `Bearer ${response.body.userJwt}`)
    .expect(201);

  return response.body.userJwt;
};

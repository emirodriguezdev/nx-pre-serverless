const express = require('express');
require('express-async-errors');

const createUserRouter = require('./routes/users/create');
const loginRouter = require('./routes/auth/login');
const createShopRouter = require('./routes/shops/create');
const editShopRouter = require('./routes/shops/edit');
const deleteShopRouter = require('./routes/shops/delete');
const createPaymentIntentionRouter = require('./routes/payment_intentions/create');
const showPaymentIntentionRouter = require('./routes/payment_intentions/show');
const createPaymentRouter = require('./routes/payments/create');

const errorHandler = require('./middlewares/error-handler');
const currentUser = require('./middlewares/current-user');

const app = express();
app.use(express.json());
app.use(currentUser);

// Auth routes
app.use(loginRouter);

// User routes
app.use(createUserRouter);

// Shop routes
app.use(createShopRouter);
app.use(editShopRouter);
app.use(deleteShopRouter);

// Payment intention routes
app.use(createPaymentIntentionRouter);
app.use(showPaymentIntentionRouter);

// Payment routes
app.use(createPaymentRouter);

app.use(errorHandler);

module.exports = app;

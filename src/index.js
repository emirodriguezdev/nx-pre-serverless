require('dotenv').config();

const mongoose = require('mongoose');
const app = require('./app');

const start = async () => {
  try {
    await mongoose.connect(process.env.MONGO_URI);
    console.log('Connected to mongo db');
  } catch (err) {
    console.log(err);
  }

  const PORT = process.env.PORT || 3000;
  app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
  });
};

start();

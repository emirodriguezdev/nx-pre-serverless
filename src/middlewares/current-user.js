const jwt = require('jsonwebtoken');

const currentUser = (req, res, next) => {
  const { authorization } = req.headers;
  if (!authorization) {
    return next();
  }

  try {
    const token = authorization.replace('Bearer ', '');
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.currentUser = decoded;
  } catch (err) {}

  next();
};

module.exports = currentUser;

const errorHandler = (err, req, res, next) => {
  res.status(400).send({
    errors: [{ message: err.message }],
  });
};

module.exports = errorHandler;

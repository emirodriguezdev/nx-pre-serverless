const mongoose = require('mongoose');

const PaymentIntentionStatuses = {
  PENDING: 'pending',
  PAID: 'paid',
  BLOCKED: 'blocked',
};

const paymentIntentionSchema = new mongoose.Schema({
  price: {
    type: Number,
    required: true,
  },
  status: {
    type: String,
    required: true,
    enum: Object.values(PaymentIntentionStatuses),
  },
  paymentAttempts: {
    type: Number,
    required: true,
    default: 0,
  },
  description: {
    type: String,
    required: true,
  },
  shopId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Shop',
    required: true,
  },
  callbackUrl: {
    type: String,
    required: true,
  },
});

const PaymentIntention = mongoose.model(
  'PaymentIntention',
  paymentIntentionSchema
);

module.exports = { PaymentIntention, PaymentIntentionStatuses };

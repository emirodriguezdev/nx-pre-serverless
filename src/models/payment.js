const mongoose = require('mongoose');

const PaymentStatuses = {
  PAID: 'paid',
  FAILED: 'failed',
};

const paymentSchema = new mongoose.Schema({
  paymentIntentionId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'PaymentIntention',
    required: true,
  },
  status: {
    type: String,
    required: true,
    enum: Object.values(PaymentStatuses),
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  callbackUrl: {
    type: String,
    required: true,
  },
});

const Payment = mongoose.model('Payment', paymentSchema);

module.exports = { Payment, PaymentStatuses };

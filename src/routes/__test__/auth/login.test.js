const request = require('supertest');
const app = require('./../../../app');

it('returns 400 with invalid email', async () => {
  await request(app)
    .post('/api/auth/login')
    .send({
      email: 'test',
      password: 'password',
    })
    .expect(400);
});

it('returns 400 with empty password', async () => {
  await request(app)
    .post('/api/auth/login')
    .send({
      email: 'test@test.com',
      password: '',
    })
    .expect(400);
});

it('fails when an email that does not exist is supplied', async () => {
  await request(app)
    .post('/api/auth/login')
    .send({
      email: 'test@test.com',
      password: 'password',
    })
    .expect(400);
});

it('fails when an incorrect password is supplied', async () => {
  await request(app)
    .post('/api/auth/login')
    .send({
      email: 'test@test.com',
      password: 'password',
    })
    .expect(400);
});

it('responds with a jwt when given valid credentials', async () => {
  await request(app)
    .post('/api/users')
    .send({
      email: 'test@test.com',
      password: 'password',
    })
    .expect(201);

  const response = await request(app)
    .post('/api/auth/login')
    .send({
      email: 'test@test.com',
      password: 'password',
    })
    .expect(200);

  expect(response.body.userJwt).toBeDefined();
});

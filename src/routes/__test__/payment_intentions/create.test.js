const request = require('supertest');
const app = require('../../../app');

it('returns a 401 if the user is not authenticated', async () => {
  await request(app)
    .post('/api/payment-intentions')
    .send({
      description: 'test',
      price: 10,
      callbackUrl: 'http://test.local:3000/callback',
    })
    .expect(401);
});

it('returns a 400 if the user provides an empty description', async () => {
  const jwt = await global.signin();

  await request(app)
    .post('/api/payment-intentions')
    .send({
      description: '',
      price: 10,
      callbackUrl: 'http://test.local:3000/callback',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(400);
});

it('returns a 400 if the user provides an empty price', async () => {
  const jwt = await global.signin();

  await request(app)
    .post('/api/payment-intentions')
    .send({
      description: 'test',
      price: '',
      callbackUrl: 'http://test.local:3000/callback',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(400);
});

it('returns a 400 if the user provides a negative price', async () => {
  const jwt = await global.signin();

  await request(app)
    .post('/api/payment-intentions')
    .send({
      description: 'test',
      price: -10,
      callbackUrl: 'http://test.local:3000/callback',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(400);
});

it('creates a payment intention without having a shop created', async () => {
  const jwt = await global.signin();

  const response = await request(app)
    .post('/api/payment-intentions')
    .send({
      description: 'test',
      price: 10,
      callbackUrl: 'http://test.local:3000/callback',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(401);

  expect(response.body.errors[0].message).toEqual(
    'You must have a shop to create a payment intention'
  );
});

it('creates a payment intention with valid inputs', async () => {
  const jwt = await global.signin();

  await request(app)
    .post('/api/shops')
    .send({
      name: 'test',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(201);

  const response = await request(app)
    .post('/api/payment-intentions')
    .send({
      description: 'test payment intention',
      price: 10,
      callbackUrl: 'http://test.local:3000/callback',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(201);

  expect(response.body.description).toEqual('test payment intention');
  expect(response.body.price).toEqual(10);
  expect(response.body.shopId).toBeDefined();
  expect(response.body.status).toEqual('pending');
  expect(response.body.paymentAttempts).toEqual(0);
});

const request = require('supertest');
const app = require('../../../app');

it('returns a 401 if the user is not authenticated', async () => {
  await request(app).get('/api/payment-intentions/123').expect(401);
});

it('returns a 404 if the payment intention is not found', async () => {
  const jwt = await global.signin();

  await request(app)
    .get('/api/payment-intentions/63220e34359b6cb0d9e4b96c')
    .set('Authorization', `Bearer ${jwt}`)
    .expect(404);
});

it('returns a 200 if the payment intention is found', async () => {
  const jwt = await global.signin();

  await request(app)
    .post('/api/shops')
    .send({
      name: 'test',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(201);

  const response = await request(app)
    .post('/api/payment-intentions')
    .send({
      description: 'test',
      price: 10,
      callbackUrl: 'http://test.local:3000/callback',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(201);

  const paymentIntentionId = response.body._id;

  const piResponse = await request(app)
    .get(`/api/payment-intentions/${paymentIntentionId}`)
    .set('Authorization', `Bearer ${jwt}`)
    .expect(200);

  expect(piResponse.body.description).toEqual('test');
  expect(piResponse.body.price).toEqual(10);
});

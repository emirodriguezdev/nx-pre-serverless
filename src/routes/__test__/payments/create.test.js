const request = require('supertest');
const app = require('./../../../app');

const createPaymentIntention = async () => {
  const jwt = await global.signinShop();

  const response = await request(app)
    .post('/api/payment-intentions')
    .send({
      description: 'test payment intention',
      price: 10,
      callbackUrl: 'http://test.local:3000/callback',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(201);

  return response.body._id;
};

it('returns 400 with missing payment intention id', async () => {
  const jwt = await global.signin();

  await request(app)
    .post('/api/payments')
    .send({ paymentIntentionId: '' })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(400);
});

it('returns 400 with missing callbackUrl', async () => {
  const jwt = await global.signin();
  await request(app)
    .post('/api/payments')
    .send({ paymentIntentionId: '63220e34359b6cb0d9e4b96c', callbackUrl: '' })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(400);
});

it('returns 400 with missing credit card data', async () => {
  const jwt = await global.signin();

  await request(app)
    .post('/api/payments')
    .send({
      paymentIntentionId: '63220e34359b6cb0d9e4b96c',
      callbackUrl: 'http://test.local:3000/callback',
      creditCard: {},
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(400);
});

it('returns errors with expired credit card', async () => {
  const jwt = await global.signin();

  const paymentIntentionId = await createPaymentIntention();

  const response = await request(app)
    .post('/api/payments')
    .send({
      paymentIntentionId,
      callbackUrl: 'http://test.local:3000/callback',
      creditCard: {
        number: '4242424242424242',
        expiryMonth: 1,
        expiryYear: 2000,
        cvv: '123',
        name: 'Test test',
      },
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(400);

  expect(response.body.errors[0].message).toEqual('Credit card is expired');
});

it('returns errors with credit card without funds', async () => {
  const jwt = await global.signin();

  const paymentIntentionId = await createPaymentIntention();

  const today = new Date();
  const response = await request(app)
    .post('/api/payments')
    .send({
      paymentIntentionId,
      callbackUrl: 'http://test.local:3000/callback',
      creditCard: {
        number: '4000000000000002',
        expiryMonth: today.getMonth() + 1,
        expiryYear: today.getFullYear() + 1,
        cvv: '123',
        name: 'NO_FUNDS',
      },
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(400);

  expect(response.body.errors[0].message).toEqual('Credit card has no funds');
});

it('returns errors with credit card blocked', async () => {
  const jwt = await global.signin();

  const paymentIntentionId = await createPaymentIntention();

  const today = new Date();
  const response = await request(app)
    .post('/api/payments')
    .send({
      paymentIntentionId,
      callbackUrl: 'http://test.local:3000/callback',
      creditCard: {
        number: '4000000000000000',
        expiryMonth: today.getMonth() + 1,
        expiryYear: today.getFullYear() + 1,
        cvv: '123',
        name: 'BLOCKED',
      },
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(400);

  expect(response.body.errors[0].message).toEqual('Credit card is blocked');
});

it('return errors with credit card with pending debt', async () => {
  const jwt = await global.signin();

  const paymentIntentionId = await createPaymentIntention();

  const today = new Date();
  const response = await request(app)
    .post('/api/payments')
    .send({
      paymentIntentionId,
      callbackUrl: 'http://test.local:3000/callback',
      creditCard: {
        number: '4000000000000341',
        expiryMonth: today.getMonth() + 1,
        expiryYear: today.getFullYear() + 1,
        cvv: '123',
        name: 'PENDING_DEBT',
      },
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(400);

  expect(response.body.errors[0].message).toEqual(
    'Credit card has pending debt'
  );
});

it('returns 200 with valid credit card', async () => {
  const jwt = await global.signin();

  const paymentIntentionId = await createPaymentIntention();

  const today = new Date();
  await request(app)
    .post('/api/payments')
    .send({
      paymentIntentionId,
      callbackUrl: 'http://test.local:3000/callback',
      creditCard: {
        number: '4242424242424242',
        expiryMonth: today.getMonth() + 1,
        expiryYear: today.getFullYear() + 1,
        cvv: '123',
        name: 'APRO',
      },
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(201);
});

it('returns 400 with failed payment', async () => {
  const jwt = await global.signin();

  const paymentIntentionId = await createPaymentIntention();

  const today = new Date();
  await request(app)
    .post('/api/payments')
    .send({
      paymentIntentionId,
      callbackUrl: 'http://test.local:3000/callback',
      creditCard: {
        number: '4242424242424242',
        expiryMonth: today.getMonth() + 1,
        expiryYear: today.getFullYear() + 1,
        cvv: '123',
        name: 'TEST',
      },
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(400);
});

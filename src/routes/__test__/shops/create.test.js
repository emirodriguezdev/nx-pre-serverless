const request = require('supertest');
const app = require('./../../../app');

it('returns 401 if the user is not authenticated', async () => {
  await request(app).post('/api/shops').send({}).expect(401);
});

it('returns 400 with missing name', async () => {
  const userJwt = await global.signin();

  await request(app)
    .post('/api/shops')
    .send({})
    .set('Authorization', `Bearer ${userJwt}`)
    .expect(400);
});

it('returns a 201 on successful creation', async () => {
  const userJwt = await global.signin();

  await request(app)
    .post('/api/shops')
    .send({
      name: 'test',
    })
    .set('Authorization', `Bearer ${userJwt}`)
    .expect(201);
});

it('cannot create two shops by same user', async () => {
  const userJwt = await global.signin();

  await request(app)
    .post('/api/shops')
    .send({
      name: 'test',
    })
    .set('Authorization', `Bearer ${userJwt}`)
    .expect(201);

  await request(app)
    .post('/api/shops')
    .send({
      name: 'test2',
    })
    .set('Authorization', `Bearer ${userJwt}`)
    .expect(400);
});

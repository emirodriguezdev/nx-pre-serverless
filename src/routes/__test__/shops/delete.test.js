const request = require('supertest');
const app = require('../../../app');

it('returns a 404 if the shop is not found', async () => {
  const jwt = await global.signin();

  await request(app)
    .put('/api/shops/63220e34359b6cb0d9e4b96c')
    .send({
      name: 'test',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(404);
});

it('returns a 401 if the user is not authenticated', async () => {
  await request(app)
    .put('/api/shops/63220e34359b6cb0d9e4b96c')
    .send({})
    .expect(401);
});

it('returns a 401 if the user does not own the shop', async () => {
  const jwt = await global.signin();
  const response = await request(app)
    .post('/api/shops')
    .send({
      name: 'test',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(201);
  const shopId = response.body._id;

  const newUserResponse = await request(app)
    .post('/api/users')
    .send({
      email: 'test2@test.com',
      password: 'pasword',
    })
    .expect(201);

  const newUserJwt = newUserResponse.body.userJwt;

  await request(app)
    .delete(`/api/shops/${shopId}`)
    .set('Authorization', `Bearer ${newUserJwt}`)
    .expect(401);
});

it('returns 200 if the user delete correctly a shop', async () => {
  const jwt = await global.signin();
  const response = await request(app)
    .post('/api/shops')
    .send({
      name: 'test',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(201);
  const shopId = response.body._id;

  await request(app)
    .delete(`/api/shops/${shopId}`)
    .set('Authorization', `Bearer ${jwt}`)
    .expect(200);
});

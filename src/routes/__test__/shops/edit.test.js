const request = require('supertest');
const app = require('../../../app');

it('returns a 404 if the shop is not found', async () => {
  const jwt = await global.signin();

  await request(app)
    .put('/api/shops/63220e34359b6cb0d9e4b96c')
    .send({
      name: 'test',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(404);
});

it('returns a 401 if the user is not authenticated', async () => {
  await request(app)
    .put('/api/shops/63220e34359b6cb0d9e4b96c')
    .send({})
    .expect(401);
});

it('returns a 401 if the user does not own the shop', async () => {
  const jwt = await global.signin();
  const response = await request(app)
    .post('/api/shops')
    .send({
      name: 'test',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(201);
  const shopId = response.body._id;

  const newUserResponse = await request(app)
    .post('/api/users')
    .send({
      email: 'test2@test.com',
      password: 'pasword',
    })
    .expect(201);

  const newUserJwt = newUserResponse.body.userJwt;

  await request(app)
    .put(`/api/shops/${shopId}`)
    .send({
      name: 'new name',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(200);
});

it('returns a 400 if the user provides an empty name', async () => {
  const jwt = await global.signin();

  await request(app)
    .put(`/api/shops/63220e34359b6cb0d9e4b96c`)
    .send({
      name: '',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(400);
});

it('returns a 200 if the user provides valid inputs', async () => {
  const jwt = await global.signin();

  const response = await request(app)
    .post('/api/shops')
    .send({
      name: 'test',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(201);

  await request(app)
    .put(`/api/shops/${response.body._id}`)
    .send({
      name: 'new name',
    })
    .set('Authorization', `Bearer ${jwt}`)
    .expect(200);
});

const request = require('supertest');
const app = require('./../../../app');

it('returns 400 with invalid email', async () => {
  await request(app)
    .post('/api/users')
    .send({
      email: 'test',
      password: 'password',
    })
    .expect(400);
});

it('returns 400 with invalid password', async () => {
  await request(app)
    .post('/api/users')
    .send({
      email: 'test@test.com',
      password: 'p',
    })
    .expect(400);
});

it('returns a 201 on successful signup', async () => {
  const response = await request(app)
    .post('/api/users')
    .send({
      email: 'test@test.com',
      password: 'password',
    })
    .expect(201);

  expect(response.body.userJwt).toBeDefined();
});

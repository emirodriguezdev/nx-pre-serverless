const express = require('express');
const { body, validationResult } = require('express-validator');

const { User } = require('../../models/user');
const Password = require('./../../services/password');
const { generateJWT } = require('./../../services/jwt');

const router = express.Router();

router.post(
  '/api/auth/login',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .notEmpty()
      .withMessage('You must supply a password'),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { email, password } = req.body;

    const existingUser = await User.findOne({ email });
    if (!existingUser) {
      throw new Error('Invalid credentials');
    }

    const passwordsMatch = await Password.compare(
      existingUser.password,
      password
    );
    if (!passwordsMatch) {
      throw new Error('Invalid credentials');
    }

    const userJwt = generateJWT(existingUser);

    res.status(200).send({ userJwt });
  }
);

module.exports = router;

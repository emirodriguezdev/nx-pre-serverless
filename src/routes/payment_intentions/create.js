const express = require('express');

const { body, validationResult } = require('express-validator');
const requireAuth = require('../../middlewares/require-auth');

const { User } = require('../../models/user');
const { Shop } = require('./../../models/shop');
const {
  PaymentIntention,
  PaymentIntentionStatuses,
} = require('./../../models/payment-intention');

const router = express.Router();

router.post(
  '/api/payment-intentions',
  requireAuth,
  [
    body('description')
      .trim()
      .notEmpty()
      .withMessage('You must supply a description'),
    body('price')
      .isFloat({ gt: 0 })
      .withMessage('Price must be greater than 0'),
    body('callbackUrl')
      .trim()
      .notEmpty()
      .isURL()
      .withMessage('You must supply a valid callbackUrl'),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { description, price, callbackUrl } = req.body;

    const user = await User.findById(req.currentUser.id);
    if (!user.shopId) {
      return res.status(401).send({
        errors: [
          { message: 'You must have a shop to create a payment intention' },
        ],
      });
    }

    const shop = await Shop.findById(user.shopId);
    const paymentIntention = new PaymentIntention({
      description,
      price,
      shopId: shop.id,
      status: PaymentIntentionStatuses.PENDING,
      callbackUrl,
    });

    await paymentIntention.save();

    res.status(201).send(paymentIntention);
  }
);

module.exports = router;

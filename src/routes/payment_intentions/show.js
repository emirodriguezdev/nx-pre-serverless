const express = require('express');
const requireAuth = require('../../middlewares/require-auth');

const { PaymentIntention } = require('../../models/payment-intention');
const { User } = require('../../models/user');

const router = express.Router();

router.get('/api/payment-intentions/:id', requireAuth, async (req, res) => {
  const paymentIntention = await PaymentIntention.findById(req.params.id);
  const currentUser = await User.findById(req.currentUser.id);
  if (
    !paymentIntention ||
    !currentUser.shopId?.equals(paymentIntention.shopId)
  ) {
    return res
      .status(404)
      .send({ errors: [{ message: 'Payment intention not found' }] });
  }

  res.send(paymentIntention);
});

module.exports = router;

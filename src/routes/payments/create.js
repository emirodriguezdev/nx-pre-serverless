const express = require('express');

const { body, validationResult } = require('express-validator');
const requireAuth = require('../../middlewares/require-auth');

const {
  PaymentIntention,
  PaymentIntentionStatuses,
} = require('./../../models/payment-intention');
const { Payment, PaymentStatuses } = require('./../../models/payment');
const { User } = require('../../models/user');
const { validateCreditCard } = require('./../../services/credit-card');
const {
  processPayment,
  notifyPaymentStatus,
} = require('./../../services/payment');

const router = express.Router();

router.post(
  '/api/payments',
  requireAuth,
  [
    body('paymentIntentionId')
      .trim()
      .notEmpty()
      .withMessage('You must supply a payment intention'),
    body('callbackUrl')
      .trim()
      .notEmpty()
      .isURL()
      .withMessage('You must supply a valid callbackUrl'),
    body('creditCard.number')
      .trim()
      .notEmpty()
      .withMessage('You must supply a credit card number'),
    body('creditCard.expiryMonth')
      .trim()
      .notEmpty()
      .withMessage('You must supply a credit card expiry month'),
    body('creditCard.expiryYear')
      .trim()
      .notEmpty()
      .withMessage('You must supply a credit card expiry year'),
    body('creditCard.cvv')
      .trim()
      .notEmpty()
      .withMessage('You must supply a credit card cvv'),
    body('creditCard.name')
      .trim()
      .notEmpty()
      .withMessage('You must supply a credit card name'),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { paymentIntentionId, callbackUrl, creditCard } = req.body;

    const paymentIntention = await PaymentIntention.findById(
      paymentIntentionId
    );
    if (!paymentIntention) {
      return res.status(404).send({
        errors: [{ message: 'Payment intention not found' }],
      });
    }

    if (paymentIntention.status !== PaymentIntentionStatuses.PENDING) {
      return res.status(400).send({
        errors: [{ message: 'Payment is not in a payable status' }],
      });
    }

    const user = await User.findById(req.currentUser.id);
    if (user.shopId?.equals(paymentIntention.shopId)) {
      return res.status(401).send({
        errors: [{ message: 'You cannot pay your own payment intention' }],
      });
    }

    const creditCardErrors = validateCreditCard(creditCard);
    if (creditCardErrors.length > 0) {
      return res.status(400).send({
        errors: creditCardErrors,
      });
    }

    const paid = processPayment(
      paymentIntention.price,
      creditCard,
      paymentIntention.description
    );

    const payment = new Payment({
      paymentIntentionId: paymentIntention.id,
      status: paid ? PaymentStatuses.PAID : PaymentStatuses.FAILED,
      callbackUrl,
      userId: user.id,
    });
    await payment.save();

    paymentIntention.paymentAttempts += 1;

    if (paid) {
      paymentIntention.status = PaymentIntentionStatuses.PAID;
      await paymentIntention.save();

      notifyPaymentStatus(payment, paymentIntention, user);

      return res.status(201).send(payment);
    } else if (paymentIntention.paymentAttempts >= 3) {
      paymentIntention.status = PaymentIntentionStatuses.BLOCKED;

      await paymentIntention.save();
    }

    notifyPaymentStatus(payment, paymentIntention, user);

    res.status(400).send({
      errors: [{ message: 'Payment failed' }],
    });
  }
);

module.exports = router;

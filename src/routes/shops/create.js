const express = require('express');
const jwt = require('jsonwebtoken');
const { body, validationResult } = require('express-validator');
const requireAuth = require('../../middlewares/require-auth');

const { User } = require('../../models/user');
const { Shop } = require('./../../models/shop');

const router = express.Router();

router.post(
  '/api/shops',
  requireAuth,
  [body('name').trim().notEmpty().withMessage('You must supply a name')],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { name } = req.body;

    const user = await User.findById(req.currentUser.id);
    if (user.shopId) {
      throw new Error('You already have a shop');
    }

    const shop = new Shop({ name });
    await shop.save();

    user.set({ shopId: shop.id });
    await user.save();

    res.status(201).send(shop);
  }
);

module.exports = router;

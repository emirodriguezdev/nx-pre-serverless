const express = require('express');

const requireAuth = require('../../middlewares/require-auth');
const { Shop } = require('./../../models/shop');
const { User } = require('../../models/user');

const router = express.Router();
router.delete('/api/shops/:id', requireAuth, async (req, res) => {
  const shop = await Shop.findById(req.params.id);
  if (!shop) {
    return res.status(404).send({ errors: [{ message: 'Shop not found' }] });
  }

  const currentUser = await User.findById(req.currentUser.id);

  if (currentUser.shopId?.toString() !== shop.id) {
    return res
      .status(401)
      .send({ errors: [{ message: 'Not authorized to delete this shop' }] });
  }

  await shop.remove();

  currentUser.set({ shopId: undefined });
  await currentUser.save();

  res.send({});
});

module.exports = router;

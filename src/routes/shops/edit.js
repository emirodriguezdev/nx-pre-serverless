const express = require('express');

const { body, validationResult } = require('express-validator');
const requireAuth = require('../../middlewares/require-auth');
const { Shop } = require('./../../models/shop');
const { User } = require('../../models/user');

const router = express.Router();
router.put(
  '/api/shops/:id',
  requireAuth,
  [body('name').trim().notEmpty().withMessage('You must supply a name')],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const shop = await Shop.findById(req.params.id);
    if (!shop) {
      return res.status(404).send({ errors: [{ message: 'Shop not found' }] });
    }

    const currentUser = await User.findById(req.currentUser.id);

    if (currentUser.shopId.toString() !== shop.id) {
      return res
        .status(401)
        .send({ errors: [{ message: 'Not authorized to edit this shop' }] });
    }

    shop.set({ name: req.body.name });
    await shop.save();

    res.send(shop);
  }
);

module.exports = router;

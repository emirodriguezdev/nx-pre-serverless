const express = require('express');
const { body, validationResult } = require('express-validator');

const { User } = require('./../../models/user');
const Password = require('./../../services/password');
const { generateJWT } = require('./../../services/jwt');

const router = express.Router();

router.post(
  '/api/users',
  [
    body('email').isEmail().withMessage('Email must be valid'),
    body('password')
      .trim()
      .isLength({ min: 4, max: 20 })
      .withMessage('Password must be between 4 and 20 characters'),
  ],
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    const { email, password } = req.body;
    const existingUser = await User.findOne({ email });
    if (existingUser) {
      throw new Error('Email in use');
    }

    const hashedPassword = await Password.toHash(password);

    const user = new User({ email, password: hashedPassword });
    await user.save();

    const userJwt = generateJWT(user);

    res.status(201).send({ userJwt });
  }
);

module.exports = router;

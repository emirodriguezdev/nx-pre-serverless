const { validateCreditCard } = require('./../credit-card');

it('credit card is expired', () => {
  const creditCard = {
    expiryMonth: 1,
    expiryYear: 2019,
  };

  const errors = validateCreditCard(creditCard);

  expect(errors).toEqual([
    {
      message: 'Credit card is expired',
    },
  ]);
});

it('credit card has no funds', () => {
  const creditCard = {
    name: 'NO_FUNDS',
  };

  const errors = validateCreditCard(creditCard);

  expect(errors).toEqual([
    {
      message: 'Credit card has no funds',
    },
  ]);
});

it('credit card has pending debt', () => {
  const creditCard = {
    name: 'PENDING_DEBT',
  };

  const errors = validateCreditCard(creditCard);

  expect(errors).toEqual([
    {
      message: 'Credit card has pending debt',
    },
  ]);
});

it('credit card is blocked', () => {
  const creditCard = {
    name: 'BLOCKED',
  };

  const errors = validateCreditCard(creditCard);

  expect(errors).toEqual([
    {
      message: 'Credit card is blocked',
    },
  ]);
});

it('credit card is valid', () => {
  const date = new Date();

  const creditCard = {
    expiryMonth: date.getMonth() + 1,
    expiryYear: date.getFullYear(),
    name: 'TEST TEST',
  };

  const errors = validateCreditCard(creditCard);

  expect(errors).toEqual([]);
});

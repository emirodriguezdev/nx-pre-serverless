const { generateJWT } = require('./../jwt');

it('returns a jwt', async () => {
  const jwt = generateJWT({ id: '123', email: 'test@test.com' });

  expect(jwt).toBeDefined();
});

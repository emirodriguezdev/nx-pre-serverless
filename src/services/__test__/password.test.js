const Password = require('./../password');

it('returns true if password matches', async () => {
  const password = await Password.toHash('password');
  const result = await Password.compare(password, 'password');

  expect(result).toBe(true);
});

it('returns false if password does not match', async () => {
  const password = await Password.toHash('password');
  const result = await Password.compare(password, 'password1');

  expect(result).toBe(false);
});

it('return hashed password ok', async () => {
  const password = await Password.toHash('password');
  expect(password).toBeDefined();
});

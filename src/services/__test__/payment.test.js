const { processPayment } = require('./../payment');

it('returns true if payment is approved', async () => {
  const result = await processPayment(100, { name: 'APRO' }, 'Test');

  expect(result).toBe(true);
});

it('returns false if payment is not approved', async () => {
  const result = await processPayment(100, { name: 'TEST TEST' }, 'Test');

  expect(result).toBe(false);
});

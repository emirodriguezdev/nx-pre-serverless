const creditCardIsExpired = (creditCard) => {
  const { expiryMonth, expiryYear } = creditCard;

  const expiryDate = new Date(expiryYear, expiryMonth);
  const today = new Date();
  return expiryDate < today;
};

const creditCardHasFunds = (creditCard) => {
  const { name } = creditCard;
  return name !== 'NO_FUNDS';
};

const creditCardHasPendingDebt = (creditCard) => {
  const { name } = creditCard;
  return name === 'PENDING_DEBT';
};

const creditCardIsActive = (creditCard) => {
  const { name } = creditCard;
  return name !== 'BLOCKED';
};

// This is a fake service that we will use to simulate a credit card service
const validateCreditCard = (creditCard) => {
  const errors = [];

  if (creditCardIsExpired(creditCard)) {
    errors.push({
      message: 'Credit card is expired',
    });
  }

  if (!creditCardHasFunds(creditCard)) {
    errors.push({
      message: 'Credit card has no funds',
    });
  }

  if (creditCardHasPendingDebt(creditCard)) {
    errors.push({
      message: 'Credit card has pending debt',
    });
  }

  if (!creditCardIsActive(creditCard)) {
    errors.push({
      message: 'Credit card is blocked',
    });
  }

  return errors;
};

module.exports = { validateCreditCard };

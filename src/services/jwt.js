const jwt = require('jsonwebtoken');

const generateJWT = (user) => {
  const userJwt = jwt.sign(
    {
      id: user.id,
      email: user.email,
    },
    process.env.JWT_SECRET
  );

  return userJwt;
};

module.exports = { generateJWT };

const bcrypts = require('bcryptjs');

module.exports = class Password {
  static async toHash(password) {
    const salt = await bcrypts.genSalt(10);
    return await bcrypts.hash(password, salt);
  }

  static async compare(storedPassword, suppliedPassword) {
    return bcrypts.compare(suppliedPassword, storedPassword);
  }
};

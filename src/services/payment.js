const { default: axios } = require('axios');

// This is a fake function that we will use to simulate a payment service
const processPayment = (price, creditCard, description) => {
  if (creditCard.name === 'APRO') {
    return true;
  }

  return false;
};

const notifyPaymentStatus = (payment, paymentIntention, user) => {
  const data = {
    paymentIntention: {
      id: paymentIntention.id,
      status: paymentIntention.status,
    },
    paymentStatus: payment.status,
    userEmail: user.email,
  };

  axios.post(paymentIntention.callbackUrl, data).catch((err) => {
    console.log('Error notifying payment status to shop', err);
  });
  axios.post(payment.callbackUrl, data).catch((err) => {
    console.log('Error notifying payment status to user', err);
  });

  return payment;
};

module.exports = { processPayment, notifyPaymentStatus };
